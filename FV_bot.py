import tweepy
from random import randint, choice
from time import sleep
from keys import *

#List of  search terms for Favorits.
FV_List = ["#linux", "#linux", "#linux"]

#Set the minimum wait time in seconds.
min_time = 780                                  #13 minuets

#Set the maximum wait time in seconds, must be equal to or higher than min_time
max_time = 1020                                 #17 minuets

#Twitter authentication
auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
auth.set_access_token(ACCESS_KEY, ACCESS_SECRET)
api = tweepy.API(auth)

def fv():                                       #Favourite function
    tag = choice(FV_List)                       #Determine a random list element
    try:
        for tweet in tweepy.Cursor(api.search, q=tag, lang="en").items(1):
            print("\n%s Tweet by: @" % tag + tweet.user.screen_name)
            tweet.favorite()
            print("Favorited the tweet")        #Print a confirmation
            if not tweet.user.following:        #If we don't follow the user
                tweet.user.follow()             #Follow the user
                print("Followed the user")
    except tweepy.TweepError as e:              #Check for an error
        print(e.reason)
    sleep(randint(min_time, max_time))          #Sleep for random amount of time

def run():                                      #Looping function
    while 1:                                    #Keep the loop running
        fv()                                    #Run the Favorite function

run()                                           #Start the loop
