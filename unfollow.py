import tweepy
from time import sleep
from keys import *

auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
auth.set_access_token(ACCESS_KEY, ACCESS_SECRET)
api = tweepy.API(auth)

followers = api.followers_ids(SCREEN_NAME)
friends = api.friends_ids(SCREEN_NAME)

for f in friends:
    try:
        if f not in followers:
            print("Unfollow {0}".format(api.get_user(f).screen_name))
            api.destroy_friendship(f)
            sleep(1)
    except tweepy.TweepError as e:
        print(e.reason)